# -*- coding: utf-8 -*-
""" Parhomenco Kirill 1933830
Python lab 01, main purpose is to simplify grading process for true/false tests
19FEB2021
Main script that invokes classes and functions
"""
import my_file_io as mfi
import process_scores as ps
import matplotlib.pyplot as plt
import pandas as pd

# Create an object from FileIO called inputFile that reads data from scores.csv.
input_file = mfi.FileIO("scores.csv")
input_file.read_from_file()

# Create an object from Result called result that uses data read from step a.
# and the correct_answer='FFTTTTFTTT'
result = ps.Result(input_file.get_records(), 'FFTTTTFTTT')

# Use object result to update the scores
result.update_score_list()

# Create an object from FileIO called reportFile that creates a report text file called
# report_scores.txt.
report_file = mfi.FileIO('report_scores.txt')

# Use object result to generate score report and save them in the report_scores.txt file.
result.generate_score_report()
d = pd.DataFrame(result.score_report)
d[2] = d[2].str.split().str.join(' ')  # a silly solution to a silly problem
d[1] = d[1].str.split().str.join(' ')
result.score_report = d.values.tolist()
report_file.write_to_file(result.score_report)

# Use object result to sort the scores and display them
result.sort_scores()
result.display_list()

# Use object result to categorize the scores based on grades and display them.
result.categorize_scores()


#  Attempt at bonus part, it works!
# def grade_counter(student_list):
#     dict_of_grades = {}
#     for grade in student_list:
#         if grade not in dict_of_grades:
#             dict_of_grades[grade] = 1
#         else:
#             dict_of_grades[grade] += 1
#     return dict_of_grades
#
#
# all_grades = []
# for item in result.score_list:
#     all_grades.append(item[4])
# dictionary = grade_counter(all_grades)
# plt.bar(range(len(dictionary)), list(dictionary.values()), align='center')
# plt.xticks(range(len(dictionary)), list(dictionary.keys()))
# plt.show()
