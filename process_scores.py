# -*- coding: utf-8 -*-
""" Parhomenco Kirill 1933830
Python lab 01, main purpose is to simplify grading process for true/false tests
19FEB2021
This file takes care of operations with list of grades: checking, marking, displaying
"""


# counting mismatches between answers
def match(str1, str2):
    mismatch_count = 0
    for i in range(10):
        if str1[i] != str2[i]:
            mismatch_count += 1
    return mismatch_count


# Since I am using this cascade of if elif, the score will just fall through
# until it hit its corresponding letter
def compute_grade(score):
    if score >= 90:
        return 'A'
    elif score >= 80:
        return 'B'
    elif score >= 70:
        return 'C'
    elif score >= 60:
        return 'D'
    else:
        return 'F'


# Solution was found on official docs.python page: https://docs.python.org/3/howto/sorting.html
def sort_field_score(lit_to_sort):
    return sorted(lit_to_sort, key=lambda x: x[3], reverse=True)


class Result:
    answer_list = []  # holds raw data from student answers page
    score_list = []  # Does not contain answers, but grades and scores
    score_report = []  # Formatted header
    correct_answer = ''  # correct 10 letter combination

    def __init__(self, answer_list, correct_answer):
        self.answer_list = answer_list
        self.correct_answer = correct_answer

    # grades students and adds score and grade column to row
    def update_score_list(self):
        for i in self.answer_list:
            if i[0] == 'refNber':
                continue
            grade = 100 - 10 * match(i[3], self.correct_answer)
            grade_letter = compute_grade(grade)
            i.pop()
            i.extend([grade, grade_letter])
            self.score_list.append(i)

    # generates finalized list with header
    def generate_score_report(self):
        # headline = self.answer_list[0] # if we want to make the headline descriptive of rows
        # headline[3] = 'score'
        # headline.append('grade')
        # lab headline
        headline = ['-------', 'CORRECT ANSWER', '--------: ', self.correct_answer, ' -------']
        self.score_report.append(headline)
        self.score_report.extend(self.score_list)

    # Sorts the private score list when invoked
    def sort_scores(self):
        self.score_list = sort_field_score(self.score_list)

    # Display in a use friendly format the students from the list along with their grades
    def display_list(self):
        for student in self.score_list:
            print(f'ID: {student[1]}, Name: {student[2]}, score: {student[3]}, grade: {student[4]}')

    # Function that displays students within certain categories or grades
    def categorize_scores(self):
        for i in 'ABCDF':
            print(f'Students in the {i} bucket: ')
            for stud in self.score_list:
                if stud[4] == i:
                    print(stud[1], '  ', stud[4], '  ', stud[3])
