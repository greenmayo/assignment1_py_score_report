# -*- coding: utf-8 -*-
""" Parhomenco Kirill 1933830
Python lab 01, main purpose is to simplify grading process for true/false tests
19FEB2021
This file takes care of the input/output of the script and handles
reading/writing of files
"""
import pandas as pd

class FileIO:
    __records = []
    __filename = ''

    # __fileobj = 'scores.csv' # not sure why this is here

    def __init__(self, fname):
        self.__filename = fname
        self.__records = []

    def read_from_file(self):
        # pandas method
        # df = pd.read_csv(self.__filename)
        # self.__records = df

        # method using open and split
        try:
            data = open(self.__filename)
            for line in data:
                listy = line.split(',')
                self.__records.append(listy)
        except FileNotFoundError:
            print("File was not found")

    def write_to_file(self, list_to_txt):  # passing the score report
        with open(self.__filename, 'w') as f:
            for item in list_to_txt:
                f.write(("%s\n" % item).replace(u'\xa0', u' '))

    def get_records(self):
        return self.__records
